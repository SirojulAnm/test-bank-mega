<?php

use App\models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'username' => 'maker1',
            'password' => bcrypt('12345'),
            'role_id'  => 1,
        ]);

        User::create([
            'username' => 'maker2',
            'password' => bcrypt('12345'),
            'role_id'  => 1,
        ]);

        User::create([
            'username' => 'approver1',
            'password' => bcrypt('12345'),
            'role_id'  => 2,
        ]);
    }
}
