<?php

use App\models\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'rolename' => 'maker',
        ]);

        Role::create([
            'rolename' => 'approver',
        ]);
    }
}
