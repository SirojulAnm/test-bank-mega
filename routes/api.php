<?php

use Illuminate\Http\Request;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', [UserController::class, 'login']); 
Route::post('createDocument', [UserController::class, 'createDocument'])->middleware('jwt.verify');  
Route::get('getDocument', [UserController::class, 'getDocument'])->middleware('jwt.verify'); 
Route::get('getDocument/{id}', [UserController::class, 'document'])->middleware('jwt.verify');
Route::put('changeStatusDocument/{id}', [UserController::class, 'changeStatusDocument'])->middleware('jwt.verify');
