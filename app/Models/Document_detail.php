<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Document_detail extends Model
{
    protected $table = 'document_detail';

    protected $fillable = [
        'document_id',
        'nama_nasabah',
        'amount',
    ];
}
