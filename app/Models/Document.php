<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $table = 'document';

    protected $fillable = [
        'document_subject',
        'status',
        'remark',
        'created_by',
        'updated_by',
    ];
}
