<?php

namespace App\Http\Controllers;

use JWTAuth;
use App\models\User;
use App\models\Document;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;

class UserController extends Controller
{
    public function login(Request $request)
    {
        $credentials = $request->only('username', 'password');
        $user = User::where('username', '=', $credentials['username'])->first();
        JWTAuth::fromUser($user, ['role' => $user->role_id]);

        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        return response()->json(compact('token'));
    }

    public function getAuthenticatedUser()
    {
        try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());

        }

        return response()->json(compact('user'));
    }

    public function createDocument(Request $request)
    {
        $document = new Document;
        $document->document_subject = $request->document_subject;
        $document->status       = $request->status;
        $document->remark       = $request->remark;
        $document->created_by   = $request->created_by;
        $document->updated_by   = $request->updated_by;
        $document->save();

        $data = [
            "status" => "201",
            "data"   => $document
        ]; 

        return response()->json($data);
    }

    public function getDocument()
    {
        $document = Document::all();

        $data = [
            "status" => "200",
            "data"   => $document
        ]; 

        return response()->json($data);
    }

    public function document($id)
    {
        $document = Document::find($id);

        $data = [
            "status" => "200",
            "data"   => $document
        ]; 

        return response()->json($data);
    }

    public function changeStatusDocument(Request $request, $id)
    {
        $document = Document::find($id);
        $document->status = $request->status;
        $document->save();

        $data = [
            "status" => "200",
            "data"   => $document
        ]; 

        return response()->json($data);
    }
}
